﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Folmbuild.UI {
    public class ProgressDialogManager: MonoBehaviour {
        [SerializeField]
        private GameObject panel;

        [SerializeField]
        private TextMeshProUGUI _text;

        public bool isShow = false;

        #region animation attributes
        public RectTransform _mainIcon;
        public float _timeStep = 0.05f;
        public float _oneStepAngle = 36;

        float _startTime;
        #endregion

        private static ProgressDialogManager _instance;

        public static ProgressDialogManager Instance() {
            if (_instance == null) {
                LoadPrefabFromResource();
            }
            return _instance;
        }

        private static void LoadPrefabFromResource() {
            GameObject goCanvas =  Instantiate(Resources.Load("Prefab/Canvas") as GameObject);
            DontDestroyOnLoad(goCanvas);
            GameObject go = Instantiate(Resources.Load("Prefab/[ProgressDialogManager]") as GameObject, goCanvas.transform);
            _instance = go.GetComponent<ProgressDialogManager>();
        }

        public void Awake() {
            if (_instance != null && _instance != this) {
                Destroy(this.gameObject);
            } else {
                _instance = this;
                DontDestroyOnLoad(this.gameObject);
            }
        }

        public void Show() {
            _text.text = "";
            panel.SetActive(true);
            isShow = true;
        }

        public void Show(string text) {
            _text.text = text;
            Show();
        }

        public void ShowWithLoading() {
            _text.text = "Loading..";
            Show();
        }

        public void SetTextInfo(string text) {
            _text.text = text;
        }

        public void Hide() {
            panel.SetActive(false);
            isShow = false;
        }

        // Update is called once per frame
        void Update() {
            if (isShow) {
                if (Time.time - _startTime >= _timeStep) {
                    Vector3 iconAngle = _mainIcon.localEulerAngles;
                    iconAngle.z += _oneStepAngle;
                    _mainIcon.localEulerAngles = iconAngle;
                    _startTime = Time.time;
                }
            }
        }
    }
}
