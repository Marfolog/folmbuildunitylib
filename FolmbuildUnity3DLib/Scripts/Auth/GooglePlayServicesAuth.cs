using Folmbuild.Auth;
using Folmbuild.Utils;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using System;
using System.Threading.Tasks;
using UnityEngine;

namespace Folmbuild.Auth {
#if FOLMBUILD_GOOGLE_PLAY_SEVICES_AUTH && UNITY_ANDROID //PlayGamesClientConfiguration just for Android platform 
    public class GooglePlayServicesAuth: AFolmbuildFirebaseAuth {

        private const bool DEBUG_LOG =  true;
        private const bool FIREBASE_USE = true;
        private  SignInStatus _currentSignInSttaus;
       

        public override async Task Initialize() {
            InitState = FolmBuildAuthState.INITIALIZING;
            try {
                if (FIREBASE_USE) {
                    FirebaseInitialize();
                }

                var config  = new PlayGamesClientConfiguration.Builder()
        .RequestServerAuthCode(false /* Don't force refresh */)
        .RequestEmail()
        .Build();


                PlayGamesPlatform.InitializeInstance(config);
                PlayGamesPlatform.DebugLogEnabled = DEBUG_LOG;
                PlayGamesPlatform.Activate();
                InitState = FolmBuildAuthState.SUCCESS_INITIALIZED;
            } catch(Exception e) {
                Debug.LogError(e);
                InitState = FolmBuildAuthState.INITIALIZE_FAILED;
            }

            await Task.Yield();
        }

        public override void LogIn() {

            Debug.Log("Google Play Service login start");
            PlayGamesPlatform.Instance.Authenticate(SignInInteractivity.NoPrompt, ProcessAuthentication);
        }

        public override void SilentLogIn()
        {
            Debug.Log("Google Play Service silent login start");
            PlayGamesPlatform.Instance.Authenticate(SignInInteractivity.CanPromptOnce, ProcessAuthentication);
        }

        internal void ProcessAuthentication(SignInStatus status) {
            _currentSignInSttaus = status;
            Debug.Log("Google status authentication: " + status);
            if (status == SignInStatus.Success) {
                // Continue with Play Games Services
                LoginSuccessfull();
            } else {
                LoginError();
            }
        }

        public override IFolmbuildUser GetUser() {
            if(PlayGamesPlatform.Instance.IsAuthenticated()) {
                AFolmbuildUser user = new AFolmbuildUser();
                user.Name = PlayGamesPlatform.Instance.GetUserDisplayName();
                user.Email = PlayGamesPlatform.Instance.GetUserEmail();
                Debug.Log("Signed user (Google Play Services): \n" + user);
                return user;
            } else {
                return null;
            }    
        }


        public override void LoginError() {
            PlayerPrefs.DeleteKey(ConstantsFW.PP_IS_SAVE_GOOGLE_ACCOUNT);
            FolmbuildErrors errors = new FolmbuildErrors();

            switch (_currentSignInSttaus) {
                case SignInStatus.NetworkError:
                    errors.Add(FolmbuildErrors.NETWORK_ERROR);
                    break;
                case SignInStatus.Failed:
                    errors.Add(FolmbuildErrors.FAILED);
                    break;
                case SignInStatus.InternalError:
                    errors.Add(FolmbuildErrors.INTERNAL_ERROR);
                    break;
                case SignInStatus.UiSignInRequired:
                    PlayGamesPlatform.Instance.Authenticate(SignInInteractivity.CanPromptAlways, ProcessAuthentication);
                    return;
                case SignInStatus.Canceled:
                    errors.Add(FolmbuildErrors.CANCELED);
                    break;
                case SignInStatus.DeveloperError:
                    errors.Add(FolmbuildErrors.DEVELOPER_ERROR);
                    break;
                case SignInStatus.NotAuthenticated:
                    errors.Add(FolmbuildErrors.NOT_AUTHENTCATED);
                    break;
            }

            afterLogin.Invoke(errors);
        }

        public override async void LoginSuccessfull() {
            PlayerPrefs.SetInt(ConstantsFW.PP_IS_SAVE_GOOGLE_ACCOUNT, 1);
            FolmbuildErrors errors = null;
            if (FIREBASE_USE) {
                errors = await SignInFirebaseGooglePlayServices(PlayGamesPlatform.Instance.GetServerAuthCode());
            }
            afterLogin.Invoke(errors);
        }

        public override async Task LogOff(IFolmbuildAuth.AfterLogout afterLogout) {
            PlayerPrefs.DeleteKey(ConstantsFW.PP_IS_SAVE_GOOGLE_ACCOUNT);
            PlayGamesPlatform.Instance.SignOut();
            if (FIREBASE_USE) {
                FirebaseSignOut();
            }
            afterLogout?.Invoke(null);
            await Task.Yield();
        }


	}
#endif
}
