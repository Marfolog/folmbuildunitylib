# FolmbuildUnityLib

## Description

FolmbuildUnityLib is a Unity3D library that provides useful tools and features for game and application development. 

## Contents

- The library includes helper images and icons
- Script utilities for:
 - I/O file,
 - setting uniform text size for all components to fit the screen,
 - android vibration implementation,
 - account managment for third-party login (Firebase support)
 - audio managment
 - listener event (ActionBroadcast)
- UI components:
 - time picker
 - dialogs
## Installation

Add the library to your project in the following way:

```bash
git clone https://github.com/your-username/FolmbuildUnityLib.git
```
