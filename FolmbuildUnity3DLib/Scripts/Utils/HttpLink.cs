﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Folmbuild.Utils {
public class HttpLink : MonoBehaviour, IPointerClickHandler {
    [SerializeField]
    private string _url;

    public void OnPointerClick(PointerEventData eventData) {
#if !UNITY_EDITOR && UNITY_WEBGL
		Application.ExternalEval( "window.open(\"" + _url + "\",\"_blank\")" );
#else
        Application.OpenURL(_url);
#endif
    }
}
}
