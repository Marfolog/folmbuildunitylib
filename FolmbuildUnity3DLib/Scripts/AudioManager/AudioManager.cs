﻿using Folmbuild.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Audio Manager
/// </summary>
public class AudioManager: MonoBehaviour {

    public bool IsPlay = true;

    private float _volume = 0;

    private static AudioManager _instance = null;

    private ScriptableAudioSources _appSounds;
    // Game Instance Singleton
    public static AudioManager Instance {
        get {
            if (_instance == null) {
                _instance = GameObject.FindObjectOfType<AudioManager>();

                if (_instance == null) {
                    GameObject container = new GameObject("AudioManager");
                    _instance = container.AddComponent<AudioManager>();
                    _instance.Initialize();
                }
            }

            return _instance;
        }
    }

    private void LoadAudioResources() {
        _instance._appSounds = Resources.Load<ScriptableAudioSources>("ApplicationAudioSources");
    }

    private void Initialize() {
        LoadAudioResources();
        // if the singleton hasn't been initialized yet
        if (_instance != null && _instance != this) {
            Destroy(this.gameObject);
        }

        _instance = this;
        this.transform.parent = null;
        DontDestroyOnLoad(this.gameObject);

        if (PlayerPrefs.HasKey(ConstantsFW.PP_VOLUME)) {
            _volume = PlayerPrefs.GetFloat(ConstantsFW.PP_VOLUME);
        } else {
            _volume = 1f;
            PlayerPrefs.SetFloat(ConstantsFW.PP_VOLUME, _volume);
        }

        foreach (Sound s in _appSounds.Sounds) {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = _volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
        }

        if (_volume == 1) {
            IsPlay = true;
        } else {
            IsPlay = false;
        }
    }



    public void Play(string name) {
        Sound s = Array.Find(_appSounds.Sounds, Sound =>Sound.name ==name);
        if (s != null ) {
            s.source.Play();
        } else {
            Debug.LogError("Audio [" + name + "] is not exists");
        }
    }


    public void ToggleVolume() {
        if (IsPlay) {
            //mute
            IsPlay = false;
            _volume = 0;
            foreach (Sound s in _appSounds.Sounds) {
                s.source.volume = 0f;
            }
        } else {
            IsPlay = true;
            _volume = 1;
            foreach (Sound s in _appSounds.Sounds) {
                s.source.volume = 1f;
            }
        }
        PlayerPrefs.SetFloat(ConstantsFW.PP_VOLUME, _volume);
    }


    public void VolumeOff() {
        //mute
        IsPlay = false;
        _volume = 0;
        foreach (Sound s in _appSounds.Sounds) {
            s.source.volume = 0f;
        }    
        PlayerPrefs.SetFloat(ConstantsFW.PP_VOLUME, _volume);
    }

    public void VolumeOn() {
        IsPlay = true;
        _volume = 1;
        foreach (Sound s in _appSounds.Sounds) {
            s.source.volume = 1f;
        } 
        PlayerPrefs.SetFloat(ConstantsFW.PP_VOLUME, _volume);
    }

    internal void Stop(string v) {
        Sound s = Array.Find(_appSounds.Sounds, Sound =>Sound.name ==v);
        if (s != null) {
            s.source.Stop();
        } else {
            Debug.LogError("Audio [" + name + "] is not exists");
        }
    }
}
