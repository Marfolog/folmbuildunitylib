﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
public class SoundOnToggle: MonoBehaviour {
    public string ThemeSound;
    Toggle _toggle;
    private void Awake() {
        _toggle = GetComponent<Toggle>();
        _toggle.onValueChanged.AddListener(delegate {
            PlaySound();
        });
    }

    public void PlaySound() {
        AudioManager.Instance.Play(ThemeSound);
    }
}