﻿//using Firebase;
//using Firebase.Auth;
//using Firebase.Analytics;
//using Google;
//using System.Collections;
//using System.Collections.Generic;
//using TMPro;
//using UnityEngine;
//using System.Threading.Tasks;

//namespace Folmbuild.Utils {
//    public abstract class SignInManager: MonoBehaviour {
//        [Header("Settings")]
//        [SerializeField]
//        private string _webClientID; // v google-services - oauth_client - client_type


//        [SerializeField]
//        private TextMeshProUGUI _errorMessageCode;

//        [SerializeField]
//        private GameObject _loginPanelWithButtonsGoogleFBETC;

//        [SerializeField]
//        private LoadingPanelManager _loadingPanelManager;

//        [SerializeField]
//        private DialogPanelManager _dialogPanelManager;

//        private GoogleSignInConfiguration _googleSignInConfiguration;

//        private Task _taskLoginFirebase;


//        private IEnumerator Awake_Coroutine() {
//            _loadingPanelManager.ShowWithLoading();
//            PlayerPrefs.SetInt(ConstantsFW.PP_PRIVACY_POLICY, 1);

//#if UNITY_EDITOR
//            yield return new WaitForSeconds(1f);
//            AwakeUnityEditorWithoutSignUp();
//            FirebaseAnalytics.SetAnalyticsCollectionEnabled(true);
//#else
//            yield return StartCoroutine(CheckAndFixDependenciesAsync());
//            if (_googleSignInConfiguration == null) {
//                _googleSignInConfiguration = new GoogleSignInConfiguration {
//                    WebClientId = _webClientID,
//                    RequestIdToken = true,
//                    RequestEmail = true,
//                    RequestAuthCode = true,
//                    UseGameSignIn = false
//                };
//                GoogleSignIn.Configuration = _googleSignInConfiguration;
//            }

//            yield return StartCoroutine(AccountManager.Instance().LoadGoogleUserFromPlayerPrefs());
//            if (AccountManager.Instance().IsSignInOverGoogle) {
//                yield return StartCoroutine(SignInSilently_Coroutine());
//            } else {
//                _loadingPanelManager.Hide();
//                ShowLoginPanel();
//            }
//#endif
//        }

//        public abstract void AwakeUnityEditorWithoutSignUp();

//        public void ShowLoginPanel() {
//            _loginPanelWithButtonsGoogleFBETC.SetActive(true);
//        }

//        private void ClearField() {
//            _errorMessageCode.text = "";
//        }

//        private void AddMessage(string msg, bool showDialogWithError) {
//            _errorMessageCode.text = msg;
//            if (showDialogWithError) {
//                _dialogPanelManager.ShowOKDialog("Warning", msg, _dialogPanelManager.HideDialog);
//            }
//        }

//        /// <summary>
//        /// Metoda, která vypisuje dle kodu (parametru) chybovou hlášku
//        /// </summary>
//        /// <param name="errorCode">chyba z Firebase</param>
//        void ShowErrorMessage(AuthError errorCode, bool withDialog = false) {
//            string msg = errorCode.ToString();
//            Debug.Log("login [error]: " + msg);
//            switch (errorCode) {
//                case AuthError.AccountExistsWithDifferentCredentials:
//                    AddMessage("No access to the APP", withDialog); // = "Účet s odlišným něco.."
//                    break;
//                case AuthError.UserNotFound:
//                    AddMessage("User not found", withDialog); // = "Účet s odlišným něco.."// = "Účet s odlišným něco.."
//                    break;
//                case AuthError.Failure:
//                    AddMessage("Sign up failure", withDialog); // = "Účet s odlišným něco.."
//                    break;
//                case AuthError.TooManyRequests:
//                    AddMessage("Too many arguments", withDialog); // = "Účet s odlišným něco.."
//                    break;
//                default:
//                    AddMessage("Error: " + errorCode, withDialog); // = "Účet s odlišným něco.."
//                    break;
//            }
//        }

//        /// <summary>
//        /// Tato metoda inicializuje instanceanse fireabase pro přihlášení:
//        /// -  Asynchronně zkontroluje, zda jsou v systému přítomny všechny nezbytné závislosti pro Firebase a zda jsou v nezbytném stavu, a pokusí se je opravit, pokud nejsou.
//        /// Povalidaci je volaná metoda <see cref="GoLogin"/> pro přihlášení
//        /// </summary>
//        /// <returns>Coroutine</returns>
//        private IEnumerator CheckAndFixDependenciesAsync() {
//            var task =  FirebaseApp.CheckAndFixDependenciesAsync();
//            yield return new WaitUntil(() => task.IsCompleted);
//            FirebaseAnalytics.SetAnalyticsCollectionEnabled(true);
//            if (task.Exception != null) {
//                Firebase.FirebaseException exception = task.Exception.Flatten().InnerExceptions[0] as Firebase.FirebaseException;
//                ShowErrorMessage((AuthError)exception.ErrorCode, true);
//            }
//        }



//        /// <summary>
//        /// ANONYMOUSE SIGN IN
//        /// </summary>
//        /// <param name="email">mail</param>
//        /// <param name="password">heslo</param>
//        /// <returns></returns>
//        private IEnumerator SignInAnonymouse_Coroutine() {
//            var loginTask = AccountManager.Instance().Auth.SignInAnonymouslyAsync();
//            yield return new WaitUntil(() => loginTask.IsCanceled || loginTask.IsFaulted || loginTask.IsCompleted);
//            if (loginTask.IsCanceled || loginTask.IsFaulted) {
//                //failed
//                Firebase.FirebaseException exception = loginTask.Exception.Flatten().InnerExceptions[0] as Firebase.FirebaseException;
//                ShowErrorMessage((AuthError)exception.ErrorCode);
//            } else {
//                //success
//                SignInSuccess(SignInType.ANONYM);
//            }
//        }

//        public abstract Task SignInSuccess(SignInType type);

//        public void OnSignInGoogle_Button() {
//            ClearField();
//            _loadingPanelManager.ShowWithLoading();
//            StartCoroutine(OnSignIn_Coroutine());
//        }

//        public void SignInAnonymouse_Button() {
//            ClearField();
//            _loadingPanelManager.ShowWithLoading();
//            StartCoroutine(SignInAnonymouse_Coroutine());
//        }

//        public IEnumerator OnSignIn_Coroutine() {
//            var taskSignIn =  GoogleSignIn.DefaultInstance.SignIn();
//            yield return new WaitUntil(() => taskSignIn.IsCanceled || taskSignIn.IsFaulted || taskSignIn.IsCompleted);
//            if (taskSignIn.IsFaulted) {
//                using (IEnumerator<System.Exception> enumerator =
//                        taskSignIn.Exception.InnerExceptions.GetEnumerator()) {
//                    if (enumerator.MoveNext()) {
//                        GoogleSignIn.SignInException error =
//                        (GoogleSignIn.SignInException)enumerator.Current;
//                        Debug.Log("[Google - OnSignIn]  Error (" + error.Status + ") " + taskSignIn.Exception);

//                        ShowDialogByException(error.Status.ToString());
//                    } else {
//                        Debug.Log("[Google - OnSignIn]  Error (" + taskSignIn.Status + ") " + taskSignIn.Exception);
//                        ShowDialogByException(taskSignIn.Status.ToString());
//                    }
//                }
//            } else if (taskSignIn.IsCanceled) {
//                Debug.Log("[Google - OnSignIn]  Error (" + taskSignIn.Status + ") " + taskSignIn.Exception);
//                ShowDialogByException(taskSignIn.Status.ToString());
//            } else {
//                AccountManager.Instance().SetNewGoogleUser(taskSignIn.Result);
//                SignInSuccess(SignInType.GOOGLE);
//            }
//        }



//        public IEnumerator LoginFirebase_Coroutine(Task task) {
//            Credential credential = Firebase.Auth.GoogleAuthProvider.GetCredential(((Task<GoogleSignInUser>)task).Result.IdToken, null);
//            _taskLoginFirebase = AccountManager.Instance().Auth.SignInWithCredentialAsync(credential);
//            yield return new WaitUntil(() => _taskLoginFirebase.IsCanceled || _taskLoginFirebase.IsCompleted || _taskLoginFirebase.IsFaulted);
//        }

//        public IEnumerator SignInSilently_Coroutine() {
//            var taskSignIn =  GoogleSignIn.DefaultInstance.SignInSilently();
//            yield return new WaitUntil(() => taskSignIn.IsCanceled || taskSignIn.IsFaulted || taskSignIn.IsCompleted);
//            if (taskSignIn.IsFaulted) {
//                using (IEnumerator<System.Exception> enumerator =
//                        taskSignIn.Exception.InnerExceptions.GetEnumerator()) {
//                    if (enumerator.MoveNext()) {
//                        GoogleSignIn.SignInException error =
//                        (GoogleSignIn.SignInException)enumerator.Current;
//                        Debug.Log("[Google - SignInSilently] Error (" + error.Status + ") " + taskSignIn.Exception);
//                        ShowDialogByException(error.Status.ToString());
//                    } else {
//                        Debug.Log("[Google - SignInSilently] Error (" + taskSignIn.Status + ") " + taskSignIn.Exception);
//                        ShowDialogByException(taskSignIn.Status.ToString());
//                    }

//                }
//            } else if (taskSignIn.IsCanceled) {
//                Debug.Log("[Google - SignInSilently]  Error (" + taskSignIn.Status.ToString() + ") " + taskSignIn.Exception);
//                ShowDialogByException(taskSignIn.Status.ToString());
//            } else {
//                Debug.Log("Sign in silently in button: email: " + taskSignIn.Result.Email);
//                SignInSuccess(SignInType.GOOGLE_SILENT);
//            }
//        }

//        private void ShowDialogByException(string exception) {
//            _loadingPanelManager.Hide();
//            if (exception.ToString().Contains("NetworkError")) {
//                _dialogPanelManager.ShowOKDialog("Error", "Not logged in\n(required network)", _dialogPanelManager.HideDialog);
//            } else {
//                _dialogPanelManager.ShowOKDialog("Error", "Not Logged In", _dialogPanelManager.HideDialog);
//            }
//        }


//        public enum SignInType {
//            FACEBOOK,
//            GOOGLE_SILENT,
//            GOOGLE,
//            ANONYM,
//            TWITTER
//        }
//    }
//}