using System;
using System.Threading.Tasks;

namespace Folmbuild.Auth {
    public interface IFolmbuildAuth {

        public delegate void AfterLogin(FolmbuildErrors errors);

        public delegate void AfterLogout(FolmbuildErrors errors);

        public Task Initialize();

        public FolmBuildAuthState GetInitState();

        public void LogIn();

        public void SilentLogIn();

        public void LoginSuccessfull();

        public void LoginError();

        public Task LogOff(AfterLogout afterLogout = null);

        public void SetCallbackAfterLogin(AfterLogin callback);

        public IFolmbuildUser GetUser();
    }

    public enum FolmBuildAuthState {
        SUCCESS_INITIALIZED,
        INITIALIZING,
        NOT_INITIALIZED,
        INITIALIZE_FAILED

    }
}
