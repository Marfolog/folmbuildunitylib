﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using UnityEngine.UI;

namespace Folmbuild.UI {
    /// <summary>
    /// Script, který reprezentuje dialogový panel.
    /// Panel informuje o uložení dat, načtení dat, chybě či jiných záležitostech
    /// </summary>
    public class DialogPanelManager: MonoBehaviour {

        /// <summary>
        /// Panel
        /// </summary>
        [SerializeField]
        private GameObject panel;

        /// <summary>
        /// Panel pro možnost Yes neb oNO
        /// </summary>
        [SerializeField]
        private GameObject YesNoPanel;

        /// <summary>
        /// Panel obsahující tlačítko OK
        /// </summary>
        [SerializeField]
        private GameObject OkPanel;

        /// <summary>
        /// Titlek dialogu
        /// </summary>
        [SerializeField]
        private TextMeshProUGUI titletextField;

        /// <summary>
        /// Obsah dialogu - informace
        /// </summary>
        [SerializeField]
        private TextMeshProUGUI contentTextField;

        /// <summary>
        /// Event pro tlačítko OK
        /// </summary>
        private Action OkAction;

        /// <summary>
        /// Event pro tlačítko YES
        /// </summary>
        private Action YesAction;

        /// <summary>
        /// Event pro tlačítko NO
        /// </summary>
        private Action NoAction;

        private static DialogPanelManager _instance;

        public static DialogPanelManager Instance() {
            if(_instance == null) {
                LoadPrefabFromResource();
            }
            return _instance;
        }

        private static void LoadPrefabFromResource() {
            GameObject goCanvas =  Instantiate(Resources.Load("Prefab/Canvas") as GameObject);
            DontDestroyOnLoad(goCanvas);
            GameObject go = Instantiate(Resources.Load("Prefab/[DialogManager]") as GameObject, goCanvas.transform);
            _instance = go.GetComponent<DialogPanelManager>();
        }

        public void Awake() {
            if (_instance != null && _instance != this) {
                Destroy(this.gameObject);
            } else {
                _instance = this;
                DontDestroyOnLoad(this.gameObject);
            }
        }

        /// <summary>
        /// Metoda pro zobrazení panelu s tlačítkem OK
        /// </summary>
        /// <param name="title">titulek</param>
        /// <param name="content">obsah dialogu</param>
        /// <param name="OkAction">funkce, která se má zavolat po stisknutí na tlačítko OK</param>
        public void ShowOKDialog(string title, string content, Action OkAction) {
            this.OkAction += OkAction;
            panel.SetActive(true);
            OkPanel.SetActive(true);
            YesNoPanel.SetActive(false);
            titletextField.text = title;
            if (title == null || title.Equals("")) {
                titletextField.gameObject.SetActive(false);
            } else {
                titletextField.gameObject.SetActive(true);
            }
            contentTextField.text = content;
        }

        /// <summary>
        /// Metoda pro zobrazení panelu s tlačítkem YES NO
        /// </summary>
        /// <param name="title">titulek</param>
        /// <param name="content">obsah dialogu</param>
        /// <param name="YesAction">funkce, která se má zavolat po stisknutí na tlačítko YES</param>
        /// <param name="NoAction">funkce, která se má zavolat po stisknutí na tlačítko NO</param>
        public void ShowYesNoDialog(string title, string content, Action YesAction, Action NoAction) {
            this.YesAction += YesAction;
            this.NoAction += NoAction;
            panel.SetActive(true);
            OkPanel.SetActive(false);
            YesNoPanel.SetActive(true);
            titletextField.text = title;
            contentTextField.text = content;
            if (title.Equals("")) {
                titletextField.gameObject.SetActive(false);
            } else {
                titletextField.gameObject.SetActive(true);
            }
        }

        /// <summary>
        /// metoda pro zavření dialogu
        /// </summary>
        public void HideDialog() {
            panel.SetActive(false);
        }

        /// <summary>
        /// Metoda, která je nastavena na tlačítku YES - zavolá funkci <see cref="YesAction"/>
        /// </summary>
        public void Yes_Button() {
            if (YesAction != null) YesAction();
        }

        /// <summary>
        /// Metoda, která je nastavena na tlačítku NO - zavolá funkci <see cref="NoAction"/>
        /// </summary>
        public void No_Button() {
            if (NoAction != null) NoAction();

        }

        /// <summary>
        /// Metoda, která je nastavena na tlačítku OK - zavolá funkci <see cref="OkAction"/>
        /// </summary>
        public void Ok_Button() {
            if (OkAction != null) OkAction();
        }

    }
}
