namespace Folmbuild.Auth {
    public interface IFolmbuildUser {

        public string GetUserName();
    }
}
