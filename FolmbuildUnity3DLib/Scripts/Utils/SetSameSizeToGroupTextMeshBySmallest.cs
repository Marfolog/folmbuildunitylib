﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;

namespace Folmbuild.Utils
{
    public class SetSameSizeToGroupTextMeshBySmallest : MonoBehaviour
    {

        [SerializeField]
        private bool rememberFirstInitilize = true;

        [SerializeField]
        private TextMeshProUGUI[] Group;

        public float size = float.MaxValue;

        private bool init = false;
        private float initSize = 0;
        public IEnumerator SetSameSizeBySmallest()
        {
            size = float.MaxValue;
            for (int i = 0; i < Group.Length; i++)
            {
                Group[i].enableAutoSizing = true;
            }
            yield return null;
            if (Group != null)
            {
                foreach (var item in Group)
                {
                    if (size > item.fontSize)
                    {
                        size = item.fontSize;
                    }
                }
                if (rememberFirstInitilize && init == false)
                {
                    initSize = size;
                    init = true;
                }
                if (rememberFirstInitilize)
                {
                    for (int i = 0; i < Group.Length; i++)
                    {
                        Group[i].enableAutoSizing = false;
                        Group[i].fontSize = initSize;
                    }
                }
                else
                {
                    for (int i = 0; i < Group.Length; i++)
                    {
                        Group[i].enableAutoSizing = false;
                        Group[i].fontSize = size;
                    }
                }
            }
            else
            {
                Debug.LogError("Error [SetSameSizeToGroupTextMeshBySmallest] Group == null");
            }
        }

        public void InitializeArray(int count)
        {
            Group = new TextMeshProUGUI[count];
        }

        public void AddTextfield(int index, TextMeshProUGUI text)
        {
            if (index <= (Group.Length - 1) && index >= 0)
            {
                Group[index] = text;
            }
            else
            {
                Debug.LogError("Error [SetSameSizeToGroupTextMeshBySmallest - AddTextfield()] Added index out of range");
            }
        }

        private void OnEnable()
        {
            StartCoroutine(SetSameSizeBySmallest());
        }
    }
}
