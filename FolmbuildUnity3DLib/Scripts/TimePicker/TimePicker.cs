﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Folmbuild.Utils {
    public class TimePicker: MonoBehaviour {
        [SerializeField]
        TextMeshProUGUI _secondsField;

        [SerializeField]
        TextMeshProUGUI _minutesField;

        [SerializeField]
        private Image _imageBackgroundMin;

        [SerializeField]
        private Image _imageBackgroundSec;

        //[SerializeField]
        //TextMeshProUGUI _hoursField;

        private int _currentSeconds;
        private int _currentMinutes;
        private int _currentHours;
        private bool _isInvalid = false;


        public enum TYPE_BUTTON {
            MINUTES,
            SECONDS,
            HOURS
        }

        private void Awake() {
            LoadPlayerPrefs();
        }

        private void LoadPlayerPrefs() {
            if (PlayerPrefs.HasKey(ConstantsFW.PP_TIME_PICKER_SECONDS)) {
                _currentSeconds = PlayerPrefs.GetInt(ConstantsFW.PP_TIME_PICKER_SECONDS);
                if (_currentSeconds < 10) {
                    _secondsField.text = "0" + _currentSeconds.ToString();
                } else {
                    _secondsField.text = _currentSeconds.ToString();
                }
            } else {
                _currentSeconds = 0;
                _secondsField.text = "00";
            }
            if (PlayerPrefs.HasKey(ConstantsFW.PP_TIME_PICKER_MINUTES)) {
                _currentMinutes = PlayerPrefs.GetInt(ConstantsFW.PP_TIME_PICKER_MINUTES);
                if (_currentMinutes < 10) {
                    _minutesField.text = "0" + _currentMinutes.ToString();
                } else {
                    _minutesField.text = _currentMinutes.ToString();
                }
            } else {
                _currentMinutes = 1;
                _minutesField.text = "0" + _currentMinutes.ToString();
            }
        }

        public void SavePlayerPrefs() {
            PlayerPrefs.SetInt(ConstantsFW.PP_TIME_PICKER_SECONDS, _currentSeconds);
            PlayerPrefs.SetInt(ConstantsFW.PP_TIME_PICKER_MINUTES, _currentMinutes);
            PlayerPrefs.SetInt(ConstantsFW.PP_TIME_PICKER_HOURS, _currentHours);
        }


        public void Init(string second, string minute, string hours) {
            _currentSeconds = int.Parse(second);
            _currentMinutes = int.Parse(minute);
            _currentHours = int.Parse(hours);
            _secondsField.text = second;
            _minutesField.text = minute;
            //_hoursField.text = hours;
        }

        public int GetMinutes() {
            return int.Parse(_minutesField.text);
        }

        public int GetSeconds() {
            return int.Parse(_secondsField.text);
        }

        public void IncrementSecond_Button() {
            if (_currentSeconds == 60) return;
            _currentSeconds++;
            if (_currentSeconds < 10) {
                _secondsField.text = "0" + _currentSeconds.ToString();
            } else {
                _secondsField.text = _currentSeconds.ToString();
            }
            ValidationDuration();
        }

        public void IncrementMinutes_Button() {
            if (_currentMinutes == 60) return;
            _currentMinutes++;
            if (_currentMinutes < 10) {
                _minutesField.text = "0" + _currentMinutes.ToString();
            } else {
                _minutesField.text = _currentMinutes.ToString();
            }
            ValidationDuration();
        }


        public void DecrementSeconds_Button() {
            if (_currentSeconds == 0) return;
            _currentSeconds--;
            if (_currentSeconds < 10) {
                _secondsField.text = "0" + _currentSeconds.ToString();
            } else {
                _secondsField.text = _currentSeconds.ToString();
            }
            ValidationDuration();
        }

        public void DecrementMinutes_Button() {
            if (_currentMinutes == 0) return;
            _currentMinutes--;
            if (_currentMinutes < 10) {
                _minutesField.text = "0" + _currentMinutes.ToString();
            } else {
                _minutesField.text = _currentMinutes.ToString();
            }
            ValidationDuration();
        }

        public int GetTimeInSeconds() {
            SavePlayerPrefs();
            return _currentSeconds + (_currentMinutes * 60) + (_currentHours * 3600);
        }


        private void ValidationDuration() {
            if (_currentMinutes == 0 && _currentSeconds == 0 && _isInvalid == false) {
                _imageBackgroundMin.color = Color.red;
                _imageBackgroundSec.color = Color.red;
                _isInvalid = true;
                Handheld.Vibrate();
            } else if (_isInvalid) {

                _imageBackgroundMin.color = Color.white;
                _imageBackgroundSec.color = Color.white;
                _isInvalid = false;
            }
        }
    }
}