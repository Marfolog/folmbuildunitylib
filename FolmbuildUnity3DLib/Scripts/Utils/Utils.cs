using System;
using System.Linq.Expressions;

namespace Folmbuild.Utils
{
    public class Utils
    {
        public static string GetMemberName<T>(Expression<Func<T>> memberExpression)
        {
            MemberExpression expressionBody = (MemberExpression)memberExpression.Body;
            return expressionBody.Member.Name;
        }
    }
}
