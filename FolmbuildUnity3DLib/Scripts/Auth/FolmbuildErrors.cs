﻿using System.Collections;
using UnityEngine;
using System;
using System.Collections.Generic;

namespace Folmbuild.Auth{
    public class FolmbuildErrors {
        public List<FolmbuildError> Errors = new List<FolmbuildError>();

       public void Add(FolmbuildError error) {
            Errors.Add(error);
       }

       public bool ErrorExist() {
           return Errors.Count > 0;
       }


        //AUTHENTICATION ERRORS
        public static FolmbuildError NETWORK_ERROR = new FolmbuildError("AUTH0001", "Network error");
        public static FolmbuildError UI_SIGN_IN_REQUIRED = new FolmbuildError("AUTH0002", "UiSignInRequired");
        public static FolmbuildError DEVELOPER_ERROR = new FolmbuildError("AUTH0003", "DeveloperError");
        public static FolmbuildError INTERNAL_ERROR = new FolmbuildError("AUTH0004", "InternalError");

        public static FolmbuildError CANCELED = new FolmbuildError("AUTH0005", "Login canceled");
        public static FolmbuildError FAILED = new FolmbuildError("AUTH0006", "Login failed");
        public static FolmbuildError NOT_AUTHENTCATED = new FolmbuildError("AUTH0007", "NotAuthenticated");

        public static FolmbuildError FIREBASE_SIGN_GOOLE_PLAY_SERVICES_CANCELED = new FolmbuildError("AUTHFG001", "Canceled");
        public static FolmbuildError FIREBASE_SIGN_GOOLE_PLAY_SERVICES_FAULTED = new FolmbuildError("AUTHFG002", "Faulted");
        public static FolmbuildError FIREBASE_SIGN_FACEBOOK_CANCELED = new FolmbuildError("AUTHFF001", "Canceled");
        public static FolmbuildError FIREBASE_SIGN_FACEBOOK_FAULTED = new FolmbuildError("AUTHFF002", "Faulted");

        public FolmbuildError GetFirstError() {
           return Errors[0];
        }

        public string PrintErrors() {
            string builder = "";
            for (int i = 0; i < Errors.Count; i++) {
                builder += Errors[i] + "(Code: "+ Errors[i].Code + ")\n";
            }
            return builder;
        }

    }


    public class FolmbuildError {
        public string Code;
        public string Error;
        public Exception exception;

        public FolmbuildError(string code, string error) {
            this.Code = code;
            this.Error = error;
        }

        public FolmbuildError(FolmbuildError folmbuildError)
        {
            this.Code = folmbuildError.Code;
            this.Error = folmbuildError.Error;
        }

        public FolmbuildError AddException(Exception e) {
            exception = e;
            return this;
        }


    }
}