using Firebase;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace Folmbuild.Auth {
    public abstract class AFolmbuildFirebaseAuth: AFolmbuildAuth {


        protected FirebaseApp FirebaseApp;

        protected Firebase.Auth.FirebaseUser firebaseUser;

        public override abstract IFolmbuildUser GetUser();

        public override abstract Task Initialize();

        public override abstract void LogIn();

        public override abstract void SilentLogIn();

        public override abstract void LoginError();

        public override abstract void LoginSuccessfull();

        public override abstract Task LogOff(IFolmbuildAuth.AfterLogout afterLogout = null);


        private Firebase.Auth.FirebaseAuth _auth = null;

        public void FirebaseInitialize() {
            _auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
            CheckFirebaseDependecies();
        }

        /// <summary>
        ///  You can check for and optionally update Google Play services to the version that is required by the Firebase Unity SDK before calling any other methods in the SDK.
        /// </summary>
        /// <exception cref="NotImplementedException"></exception>
        private void CheckFirebaseDependecies() {
            Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
                var dependencyStatus = task.Result;
                if (dependencyStatus == Firebase.DependencyStatus.Available) {
                    // Create and hold a reference to your FirebaseApp,
                    // where app is a Firebase.FirebaseApp property of your application class.
                    FirebaseApp = Firebase.FirebaseApp.DefaultInstance;

                    // Set a flag here to indicate whether Firebase is ready to use by your app.
                } else {
                    UnityEngine.Debug.LogError(System.String.Format(
                      "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                    // Firebase Unity SDK is not safe to use here.
                }
            });

        }


        protected async Task<FolmbuildErrors> SignInFirebaseGooglePlayServices(string authCode, FolmbuildErrors errors = null) {
            firebaseUser = null;
            if (errors == null) {
                errors = new FolmbuildErrors();
            }

            Firebase.Auth.Credential credential = Firebase.Auth.PlayGamesAuthProvider.GetCredential(authCode);
            await _auth.SignInWithCredentialAsync(credential).ContinueWith(task => {
                if (task.IsCanceled) {
                    errors.Add(FolmbuildErrors.FIREBASE_SIGN_GOOLE_PLAY_SERVICES_CANCELED.AddException(task.Exception));
                    Debug.LogError(errors.PrintErrors());
                    Debug.LogError("SignInWithCredentialAsync was canceled.");

                } else if (task.IsFaulted) {
                    errors.Add(FolmbuildErrors.FIREBASE_SIGN_GOOLE_PLAY_SERVICES_FAULTED.AddException(task.Exception));
                    Debug.LogError(errors.PrintErrors());
                    Debug.LogError("SignInWithCredentialAsync encountered an error: " + task.Exception);
                } else {

                    Debug.Log("Firebase google play services login successful");
                    firebaseUser = task.Result;
                    Debug.LogFormat("User signed in successfully: {0} ({1})",
                        firebaseUser.DisplayName, firebaseUser.UserId);
                }
            });

            return errors;
        }

        protected async Task<FolmbuildErrors> SignInFirebaseFacebook(string accessToken, FolmbuildErrors errors = null) {
            firebaseUser = null;
            if (errors == null) {
                errors = new FolmbuildErrors();
            }

            Firebase.Auth.Credential credential =
    Firebase.Auth.FacebookAuthProvider.GetCredential(accessToken);
            await _auth.SignInWithCredentialAsync(credential).ContinueWith(task => {
                if (task.IsCanceled) {
                    errors.Add(FolmbuildErrors.FIREBASE_SIGN_FACEBOOK_CANCELED.AddException(task.Exception));
                    Debug.LogError(errors.PrintErrors());
                    Debug.LogError("SignInWithCredentialAsync was canceled.");
                } else  if (task.IsFaulted) {
                    errors.Add(FolmbuildErrors.FIREBASE_SIGN_FACEBOOK_FAULTED.AddException(task.Exception));
                    Debug.LogError(errors.PrintErrors());
                    Debug.LogError("SignInWithCredentialAsync encountered an error: " + task.Exception);
                } else {
                    Debug.Log("Firebase facebook login successful");
                    firebaseUser = task.Result;
                    Debug.LogFormat("User signed in successfully: {0} ({1})",
                        firebaseUser.DisplayName, firebaseUser.UserId);
                }
            });


            return errors;
        }


        protected void FirebaseSignOut() {
            Firebase.Auth.FirebaseAuth.DefaultInstance.SignOut();
        }
    }
}
