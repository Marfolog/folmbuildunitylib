﻿using UnityEngine;

[CreateAssetMenu(fileName = "ApplicationAudioSources", menuName = "ScriptableObjects/ScriptableAudioSources", order = 1)]
public class ScriptableAudioSources: ScriptableObject {
    [SerializeField]
    public Sound [] Sounds;
}
