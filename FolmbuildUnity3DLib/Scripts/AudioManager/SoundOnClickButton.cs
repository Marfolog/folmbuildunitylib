﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundOnClickButton : MonoBehaviour
{
    public string ThemeSound;
    Button _button;
    private void Awake() {
        _button = GetComponent<Button>();
        _button.onClick.AddListener(PlaySound);
    }

    public void PlaySound() {
        AudioManager.Instance.Play(ThemeSound);
    }
}
