﻿using System;
using System.Collections.Generic;

namespace Folmbuild.Broadcaster
{
	public delegate void MethodDelegate();
	public delegate void MethodDelegate<T>(T va);


	public static class ActionBroadcaster
	{
		private static Dictionary<string, MethodDelegate> _tableOfAcitons = new Dictionary<string, MethodDelegate>();

		public static void InvokeAction(string actionName)
		{
			if (_tableOfAcitons.ContainsKey(actionName))
			{
				_tableOfAcitons[actionName].Invoke();
			}
		}


		public static void RegisterAction(string actionName, MethodDelegate method)
		{
			if (_tableOfAcitons.ContainsKey(actionName))
			{
				_tableOfAcitons[actionName] = _tableOfAcitons[actionName] + method;
			}
			else
			{
				_tableOfAcitons.Add(actionName, method);
			}
		}

		public static void UnRegisterAction(string actionName, MethodDelegate method)
		{
			if (_tableOfAcitons.ContainsKey(actionName))
			{
				_tableOfAcitons[actionName] = _tableOfAcitons[actionName] - method;
			}
		}
	}


	public static class ActionBroadcaster<T>
	{

		private static Dictionary<string, Delegate> tableOfAcitons = new Dictionary<string, Delegate>();

		public static void InvokeAction(string actionName, T parameter)
		{
			if (tableOfAcitons.ContainsKey(actionName))
			{
				tableOfAcitons[actionName].DynamicInvoke(parameter);
			}
			else
			{
				throw new Exception(string.Format("Attempting to invoke action {0} but ActionBroadcaster doesn't know about this event name.", actionName));
			}
		}


		public static void RegisterAction(string actionName, MethodDelegate<T> method)
		{
			if (tableOfAcitons.ContainsKey(actionName))
			{
				tableOfAcitons[actionName] = (MethodDelegate<T>)tableOfAcitons[actionName] + method;
			}
			else
			{
				tableOfAcitons.Add(actionName, method);
			}
		}

		public static void UnRegisterAction(string actionName, MethodDelegate<T> method)
		{
			if (tableOfAcitons.ContainsKey(actionName))
			{
				tableOfAcitons[actionName] = (MethodDelegate<T>)tableOfAcitons[actionName] - method;
			}
		}
	}
}

