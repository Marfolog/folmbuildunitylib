using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Folmbuild.Auth {
    public class AFolmbuildUser:  IFolmbuildUser {
        public string Name;
        public string Email;
        public Texture2D Texture;

        public string GetUserName() {
            return Name;
        }

        public override string ToString()
		{
            var stringBuilder = new System.Text.StringBuilder();
            stringBuilder.Append("Name: " + Name);
            stringBuilder.Append("\nEmail: "+ Email);        
            return stringBuilder.ToString();
        }

        
    }
}
