﻿namespace Folmbuild.Utils {
    /// <summary>
    /// Třída nesoucí některé konstanty používané v této aplikaci
    /// </summary>
    public class ConstantsFW {

        public static string PP_IS_SAVE_GOOGLE_ACCOUNT = "SAVE_GOOGLE_ACCOUNT";
        public static string PP_GOOGLE_IDTOKEN = "PP_GOOGLE_IDTOKEN";
        public static string PP_GOOGLE_DISPLAYNAME = "PP_GOOGLE_DISPLAYNAME";
        public static string PP_GOOGLE_EMAIL = "PP_GOOGLE_EMAIL";
        public static string PP_GOOGLE_USERID = "PP_GOOGLE_USERID";

        public static string PP_IS_SAVE_FACEBOOK_ACCOUNT = "SAVE_FACEBOOK_ACCOUNT";


        public static string PP_PRIVACY_POLICY = "PP_PRIVACY_POLICY";

        public static string PP_VOLUME = "PP_VOLUME";

        public static string DATE_FORMAT = "yyyy-MM-dd HH:m:ss";

        public static string PP_TIME_PICKER_SECONDS = "PP_TIME_PICKER_SECONDS";
        public static string PP_TIME_PICKER_MINUTES = "PP_TIME_PICKER_MINUTES";
        public static string PP_TIME_PICKER_HOURS = "PP_TIME_PICKER_HOURS";

    }
}