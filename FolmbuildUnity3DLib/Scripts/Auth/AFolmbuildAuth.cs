﻿using System;
using System.Collections;
using System.Threading.Tasks;
using UnityEngine;

namespace Folmbuild.Auth {
    public abstract class AFolmbuildAuth: IFolmbuildAuth {

        protected FolmBuildAuthState InitState = FolmBuildAuthState.NOT_INITIALIZED;
        protected IFolmbuildAuth.AfterLogin afterLogin;

        public abstract IFolmbuildUser GetUser();
        public abstract Task Initialize();

        public FolmBuildAuthState GetInitState() {
            return InitState;
        }

        public abstract void LogIn();

        public abstract void SilentLogIn();

        public abstract void LoginError();

        public abstract void LoginSuccessfull();

        public abstract Task LogOff(IFolmbuildAuth.AfterLogout afterLogout = null);

        public void SetCallbackAfterLogin(IFolmbuildAuth.AfterLogin callback) {
            afterLogin = callback;
        }
    }
}