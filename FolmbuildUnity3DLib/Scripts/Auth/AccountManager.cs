﻿using UnityEngine;
using Folmbuild.UI;
using Folmbuild.Utils;
using System.Threading.Tasks;
using System;

namespace Folmbuild.Auth
{
	//    /// <summary>
	//    /// Třída představující správce daného přihlášeného účtu.
	//    /// Obshauje třídní atributy tržící ifnormace o účtu, typu apod.
	//    /// </summary>
	public class AccountManager
	{


		private AuthenticationType _currentAuthenticationType;

		private static AccountManager _instance;
		private IFolmbuildAuth _currentAuthentication;
		private IFolmbuildAuth.AfterLogin _afterLoginCallback;

		public AFolmbuildUser User;
		public bool IsLoggedIn;


		public static AccountManager Instance()
		{
			if (_instance == null)
			{
				_instance = new AccountManager();
			}
			return _instance;
		}

		public void LoginInitialize(IFolmbuildAuth.AfterLogin callback)
		{
			_afterLoginCallback = callback;
			if (LogedGooglePlayGame())
			{
				_ = GoogleAuthInit(true);
			}
			else if (LogedFacebook())
			{
				_ = FacebookAuthInit(true);
			}
		}

		private async Task GoogleAuthInit(bool silentLogin = false)
		{
			Debug.Log("Google initialize start");
			if (_currentAuthentication == null || !_currentAuthentication.GetInitState().Equals(FolmBuildAuthState.INITIALIZING))
			{
				ProgressDialogManager.Instance().Show();
				_currentAuthenticationType = AuthenticationType.GooglePlayGames;
				_currentAuthentication = new GooglePlayServicesAuth();
				await _currentAuthentication.Initialize();
				_currentAuthentication.SetCallbackAfterLogin(AfterLogin);
				GoogleLogin(silentLogin);
			}
			Debug.Log("Google initialize finish");
		}

		private async Task FacebookAuthInit(bool silentLogin = false)
		{
			Debug.Log("Facebook initialize start");
			if (_currentAuthentication == null || !_currentAuthentication.GetInitState().Equals(FolmBuildAuthState.INITIALIZING))
			{
				ProgressDialogManager.Instance().Show();
				_currentAuthenticationType = AuthenticationType.Facebook;
				_currentAuthentication = new FacebookAuth();
				await _currentAuthentication.Initialize();
				_currentAuthentication.SetCallbackAfterLogin(AfterLogin);
				FacebookLogin(silentLogin);
			}
			Debug.Log("Facebook initialize finish");
		}

		private void AfterLogin(FolmbuildErrors errors)
		{
			_afterLoginCallback.Invoke(errors);
			User = (AFolmbuildUser)_currentAuthentication.GetUser();
			if (errors != null && !errors.ErrorExist())
			{
				//success
				Debug.Log("AccountManager IsLoggedIn = true");
				IsLoggedIn = true;
			}
			else
			{
				//unsuccess
				Debug.Log("AccountManager IsLoggedIn = false");
				IsLoggedIn = false;
			}
		}

		private bool LogedGooglePlayGame()
		{
			if (PlayerPrefs.HasKey(ConstantsFW.PP_IS_SAVE_GOOGLE_ACCOUNT)
					   && PlayerPrefs.GetInt(ConstantsFW.PP_IS_SAVE_GOOGLE_ACCOUNT) == 1)
			{
				return true;
			}
			else return false;
		}

		private bool LogedFacebook()
		{
			if (PlayerPrefs.HasKey(ConstantsFW.PP_IS_SAVE_FACEBOOK_ACCOUNT)
					   && PlayerPrefs.GetInt(ConstantsFW.PP_IS_SAVE_FACEBOOK_ACCOUNT) == 1)
			{
				return true;
			}
			else return false;
		}


		public void GoogleLogin(bool silent = false)
		{
			ProgressDialogManager.Instance().Show();
			if (_currentAuthentication != null && _currentAuthentication.GetInitState().Equals(FolmBuildAuthState.SUCCESS_INITIALIZED) && _currentAuthentication is GooglePlayServicesAuth)
			{
				if (silent)
				{
					_currentAuthentication.SilentLogIn();
				}
				else
				{
					_currentAuthentication.LogIn();
				}
			}
			else
			{
				_ = GoogleAuthInit();
			}
		}

		public void FacebookLogin(bool silent = false)
		{
			ProgressDialogManager.Instance().Show();
			if (_currentAuthentication != null && _currentAuthentication.GetInitState().Equals(FolmBuildAuthState.SUCCESS_INITIALIZED) && _currentAuthentication is FacebookAuth)
			{
				if (silent)
				{
					_currentAuthentication.SilentLogIn();
				}
				else
				{
					_currentAuthentication.LogIn();
				}
			}
			else
			{
				_ = FacebookAuthInit();
			}
		}

		public void LogOff(IFolmbuildAuth.AfterLogout afterLogout = null)
		{
			if (_currentAuthentication != null)
			{
				_currentAuthentication.LogOff(afterLogout);
			}
			else
			{
				Debug.LogError("_currentAuthentication is null - was not login to google or facebook");
				afterLogout?.Invoke(null);
			}

		}

		public IFolmbuildUser GetUser()
		{
#if !UNITY_EDITOR
			if (_currentAuthentication != null)
			{
				return _currentAuthentication.GetUser();
			}
			else
			{
				Debug.LogError("User is null");
				return null;
			}
#else

			return createEditorUser();
#endif
		}

        private IFolmbuildUser createEditorUser() {
			AFolmbuildUser user  = new AFolmbuildUser();
			user.Name = "EditorUser";
			user.Email = "editor@folmbuil.cz";
			return user;
        }

        public enum AuthenticationType
		{
			GooglePlayGames,
			Facebook
		}

	}
}