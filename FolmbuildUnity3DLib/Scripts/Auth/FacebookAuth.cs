using Folmbuild.Auth;
using Folmbuild.Utils;
using Facebook.Unity;
using System.Threading.Tasks;
using UnityEngine;
using System.Collections.Generic;

namespace Folmbuild.Auth {
#if FOLMBUILD_FACEBOOK_AUTH
    public class FacebookAuth: AFolmbuildFirebaseAuth {

        private const bool FIREBASE_USE = true;

        private ILoginResult _loginResult;

        private Facebook.Unity.AccessToken _currentAccessToken;

        public override async Task Initialize() {
            InitState = FolmBuildAuthState.INITIALIZING;
            Debug.Log("Facebook login initializing..");
            if (FIREBASE_USE) {
                FirebaseInitialize();
            }
            if (!FB.IsInitialized) {
                // Initialize the Facebook SDK
                FB.Init(InitCallback, OnHideUnity);
            } else {
                // Already initialized, signal an app activation App Event
                Debug.Log("Facebook initialized success");
                InitState = FolmBuildAuthState.SUCCESS_INITIALIZED;
                FB.ActivateApp();
            }

            while(InitState == FolmBuildAuthState.INITIALIZING) {
                Debug.Log("Initializing Facebook...");
                await Task.Yield();
            }
            await Task.Yield();
        }


        private void InitCallback() {
            if (FB.IsInitialized) {
                InitState = FolmBuildAuthState.SUCCESS_INITIALIZED;
                // Signal an app activation App Event
                Debug.Log("Facebook initialized success");
                FB.ActivateApp();
                // Continue with Facebook SDK
                // ...
            } else {
                InitState = FolmBuildAuthState.INITIALIZE_FAILED;
                Debug.LogError("Failed to Initialize the Facebook SDK");
            }
        }

        private void OnHideUnity(bool isGameShown) {
            if (!isGameShown) {
                // Pause the game - we will need to hide
                Time.timeScale = 0;
            } else {
                // Resume the game - we're getting focus again
                Time.timeScale = 1;
            }
        }

        public override void LogIn() {
            Debug.Log("Facebook login start");
            var perms = new List<string>(){"public_profile", "email"};
            FB.LogInWithReadPermissions(perms, AuthCallback);
        }

        public override void SilentLogIn()
        {
            Debug.Log("Facebook silent login start");
            FB.Android.RetrieveLoginStatus(SilentAuthCallback);
        }

        private void SilentAuthCallback(ILoginStatusResult result)
        {
            _loginResult = result;
            Debug.Log("Facebook status silent authentication: " + result);
            if (FB.IsLoggedIn && !result.Failed)
            {
                Debug.Log("Facebook user silent login success");
                // AccessToken class will have session details
                _currentAccessToken = result.AccessToken;
                // Print current access token's User ID
                LoginSuccessfull();
            }
            else
            {
                Debug.Log("Facebook user unsuccess silent login");
                LoginError();
            }
        }

        private void AuthCallback(ILoginResult result) {
            _loginResult = result;
            Debug.Log("Facebook status authentication: " + result);
            if (FB.IsLoggedIn) {
                Debug.Log("Facebook user login success");
                // AccessToken class will have session details
                _currentAccessToken = Facebook.Unity.AccessToken.CurrentAccessToken;
                // Print current access token's User ID
                LoginSuccessfull();
            } else {
                Debug.Log("Facebook user cancelled login");
                LoginError();
            }
        }




        public override IFolmbuildUser GetUser() {
            if(FB.IsLoggedIn) {
                var profile = FB.Mobile.CurrentProfile();
                AFolmbuildUser user = new AFolmbuildUser();
                user.Name  = profile.Name;
                user.Email = profile.Email;
                Debug.Log("Signed user (Facebook) \n" + user);
                return null;
            } else {
                return null;
            }    
        }


        public override void LoginError() {
            PlayerPrefs.DeleteKey(ConstantsFW.PP_IS_SAVE_FACEBOOK_ACCOUNT);
            FolmbuildErrors errors = new FolmbuildErrors();
            if(_loginResult.Cancelled) {
                errors.Add(FolmbuildErrors.CANCELED);
            } else if(_loginResult.Error != null){
                errors.Add(new FolmbuildError("AUTHFG002", _loginResult.Error));
            }

            afterLogin.Invoke(errors);
        }

        public override async void LoginSuccessfull() {
            PlayerPrefs.SetInt(ConstantsFW.PP_IS_SAVE_FACEBOOK_ACCOUNT, 1);
            FolmbuildErrors errors = null;
            if(_currentAccessToken == null)
			{
                errors = new FolmbuildErrors();
                errors.Add(new FolmbuildError(FolmbuildErrors.FIREBASE_SIGN_FACEBOOK_FAULTED.Code, _loginResult.Error));
            }

            if (FIREBASE_USE && _currentAccessToken != null) {
                Debug.Log("Firebase logining...");
                errors = await SignInFirebaseFacebook(_currentAccessToken.TokenString);
            }
            afterLogin.Invoke(errors);
        }

        public override async Task LogOff(IFolmbuildAuth.AfterLogout afterLogout) {
            PlayerPrefs.DeleteKey(ConstantsFW.PP_IS_SAVE_FACEBOOK_ACCOUNT);
            FB.LogOut();
            _currentAccessToken = null;
            if (FIREBASE_USE) {
                FirebaseSignOut();
            }
            afterLogout?.Invoke(null);
            await Task.Yield();
        }

	
	}
#endif
}
