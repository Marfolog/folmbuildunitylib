using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(Toggle))]
public class AnimateFolmbuildToggle : MonoBehaviour
{
    [SerializeField]
    private Image _background;

    [SerializeField]
    private Image _toggleSprite;


    [SerializeField]
    private Color _colorOn = Color.green;

    [SerializeField]
    private Color _colorOff = Color.grey;


    public UnityEvent OnEventToggle;

    private Toggle _toggleComponent;

    void Awake() {
        _toggleComponent = GetComponent<Toggle>();
        _toggleComponent.onValueChanged.AddListener(delegate {
            OnValueChanged(_toggleComponent);
        });
        UpdateToggle();
    }

    private void OnEnable() {
        UpdateToggle();
    }

    private void UpdateToggle() {
        if (_toggleComponent != null && _toggleComponent.isOn) {
            SetToggleImageOn();
        } else {
            SetToggleImageOff();
        }
    }

    private void OnValueChanged(Toggle toggle) {
        bool state = toggle.isOn;
        if (state) { animateToggleOn(); } else animateToggleOff();
    }


    private void animateToggleOff() {
        SetToggleImageOff();
    }

    private void animateToggleOn() {
        SetToggleImageOn();
    }

    public void SetToggleImageOn() {
        RectTransform _toggleSpriteRectTransform = _toggleSprite.GetComponent<RectTransform>();
        _toggleSpriteRectTransform.localScale = Vector3.one;
        _toggleSprite.color = _colorOn;
    }

    public void SetToggleImageOff() {
        RectTransform _toggleSpriteRectTransform = _toggleSprite.GetComponent<RectTransform>();
        _toggleSpriteRectTransform.localScale = new Vector3(-1f, 1f ,1f);
        _toggleSprite.color = _colorOff;
    }
}
