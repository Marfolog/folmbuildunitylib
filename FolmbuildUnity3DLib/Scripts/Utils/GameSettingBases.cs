using System;
using UnityEngine;

namespace Folmbuild.Utils
{
    public class GameSettingsBase
    {
        private bool _vibrationActive;
        private bool _soundAcvite;
        private bool _adsActive;
 

        #region CONSTANTS
        private string VIBRATION_ACTIVE = "VIBRATION_ACTIVE";
        private string SOUND_ACTIVE = "SOUND_ACTIVE";
        private string ADS_ACTIVE = "ADS_ACTIVE";
        #endregion

        public bool VibrationActive { get => _vibrationActive; set => _vibrationActive = value; }

        public bool SoundActive { get => _soundAcvite; set => _soundAcvite = value; }

        public bool AdsActive { get => _adsActive; set => _adsActive = value; }


        public virtual void LoadSettingsFromPlayerPrefs()
        {

            if(ExistsBoolFromPlayerPrefs(VIBRATION_ACTIVE)) {
                _vibrationActive = GetBoolFromPlayerPrefs(VIBRATION_ACTIVE);
            } else {
                _vibrationActive = true; //default value
            }

            if (ExistsBoolFromPlayerPrefs(SOUND_ACTIVE)) {
                _soundAcvite = GetBoolFromPlayerPrefs(SOUND_ACTIVE);
            } else {
                _soundAcvite = true; //default value
            }

            _adsActive = GetBoolFromPlayerPrefs(ADS_ACTIVE);
        }

        public virtual void SaveSettingsToPlayerPrefs()
		{
            SetBoolToPlayerPrefs(_vibrationActive, VIBRATION_ACTIVE);
            SetBoolToPlayerPrefs(_soundAcvite, SOUND_ACTIVE);
            SetBoolToPlayerPrefs(_adsActive, ADS_ACTIVE);
        }

        public bool GetBoolFromPlayerPrefs(string key)
		{
           return PlayerPrefs.GetInt(key, 0) == 1 ? true : false;
        }

        public String GetStringFromPlayerPrefs(string key)
        {
            return PlayerPrefs.GetString(key, null);
        }

        public void SetStringToPlayerPrefs(string value, string key)
		{
            PlayerPrefs.SetString(key, value);
		}

        public void SetIntegerToPlayerPrefs(int value, string key)
        {
            PlayerPrefs.SetInt(key, value);
        }

        public void SetBoolToPlayerPrefs(bool value, string key)
        {
            PlayerPrefs.SetInt(key, value ? 1 : 0);
        }


        public bool ExistsBoolFromPlayerPrefs(string key) {
            return PlayerPrefs.HasKey(key);
        }
    }
}
