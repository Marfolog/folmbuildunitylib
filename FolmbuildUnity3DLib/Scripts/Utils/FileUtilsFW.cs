﻿using System;
using System.IO;
using System.Text;
using UnityEngine;

/// <summary>
/// Třída, která pracuje s JSOn souborem pro načtení lokalizačních dat ze souboru
/// </summary>
namespace Folmbuild.Utils {
    public class FileUtilsFW {
        public static T LoadData<T>(string filePath, bool withEncodig) {
            if (withEncodig) {
                return JsonUtility.FromJson<T>(Encoding.UTF8.GetString(Convert.FromBase64String(File.ReadAllText(filePath))));
            } else {
                return JsonUtility.FromJson<T>(File.ReadAllText(filePath));
            }
        }

        public static void SaveFile(string filePath, System.Object data, bool withEncodig) {
            if (withEncodig) {
                File.WriteAllText(filePath, Convert.ToBase64String(Encoding.UTF8.GetBytes(JsonUtility.ToJson(data))));
            } else {
                File.WriteAllText(filePath, JsonUtility.ToJson(data));
            }
        }

        public static T LoadiOS<T>(string path, bool withEncodig) {
            byte[] jsonBytes = File.ReadAllBytes (path);
            string dataAsJson = Encoding.ASCII.GetString (jsonBytes);
            if (withEncodig) {
                return JsonUtility.FromJson<T>(Encoding.UTF8.GetString(Convert.FromBase64String(dataAsJson)));
            } else {
                return JsonUtility.FromJson<T>(dataAsJson);
            }
        }

        public static T LoadAndroid<T>(string filePath, bool withEncodig) {
            //byte[] jsonBytes = File.ReadAllBytes (filePath);
            //string dataAsJson = Encoding.ASCII.GetString (jsonBytes);
            //return JsonUtility.FromJson<T>(Encoding.UTF8.GetString(Convert.FromBase64String(dataAsJson)));
            if (withEncodig) {
                return JsonUtility.FromJson<T>(Encoding.ASCII.GetString(Convert.FromBase64String(File.ReadAllText(filePath))));
            } else {
                return JsonUtility.FromJson<T>(File.ReadAllText(filePath));
            }
        }

        public static void WriteAndroid<T>(string filePath, T playerData, bool withEncodig) {
            //string dataAsJson = JsonUtility.ToJson (playerData, true);
            //byte[]  jsonBytes = Encoding.ASCII.GetBytes(Convert.ToBase64String(Encoding.UTF8.GetBytes(dataAsJson)));
            //File.WriteAllBytes(filePath, jsonBytes);
            if (withEncodig) {
                File.WriteAllText(filePath, Convert.ToBase64String(Encoding.ASCII.GetBytes(JsonUtility.ToJson(playerData, true))));
            } else {
                File.WriteAllText(filePath, JsonUtility.ToJson(playerData, true));
            }
        }

        public static void WriteiOS(string path, object playerData, bool withEncodig) {
            string dataAsJson = JsonUtility.ToJson (playerData, true);
            if (withEncodig) {
                byte[] jsonBytes = Encoding.ASCII.GetBytes(Convert.ToBase64String(Encoding.UTF8.GetBytes(dataAsJson)));
                File.WriteAllBytes(path, jsonBytes);
            } else {
                File.WriteAllText(path, dataAsJson);
            }

        }
        public class WorkJson {


            /// <summary>
            /// Ze streaming assets lze jen číst
            /// </summary>
            /// <param name="path"></param>
            /// <returns></returns>
            public static T LoadJSONFromStreamingAssets<T>(string path) {
                WWW reader = null;
                string filePath = null;
#if UNITY_EDITOR_OSX || UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
                filePath = System.IO.Path.Combine(Application.streamingAssetsPath, path);
                string jsonString = "";
                reader = new WWW(filePath);
                while (!reader.isDone) { }

                jsonString = reader.text;

                return JsonUtility.FromJson<T>(jsonString);
#elif UNITY_ANDROID
                filePath = Path.Combine("jar:file://" + Application.dataPath + "!assets/", path);
                string jsonString = "";
                reader = new WWW(filePath);
                while (!reader.isDone) { }

                jsonString = reader.text;

                return JsonUtility.FromJson<T>(jsonString);
#elif UNITY_IOS
                filePath = System.IO.Path.Combine(Application.streamingAssetsPath, path);
                string result = "";
                if (filePath.Contains("://")) {
                UnityEngine.Networking.UnityWebRequest www = UnityEngine.Networking.UnityWebRequest.Get(filePath);
                while (!www.isDone) { }
                result = www.downloadHandler.text;
                return JsonUtility.FromJson<T>(result);
                } else {
                result = System.IO.File.ReadAllText(filePath);
                return JsonUtility.FromJson<T>(result);
                }
#else
                filePath = Application.dataPath + "/StreamingAssets/" + path;
                StreamReader readerStream = new StreamReader(filePath);
                string json = readerStream.ReadToEnd();
                return JsonUtility.FromJson<T>(json);
#endif

            }
        }

    }
}
